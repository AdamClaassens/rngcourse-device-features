import MapView, {Marker} from "react-native-maps";
import {Alert, StyleSheet} from "react-native";
import {useCallback, useLayoutEffect, useState} from "react";
import {useNavigation} from "@react-navigation/native";
import IconButton from "../components/ui/IconButton";

const Map = ({navigation, route}) => {
    const initialLocation = route.params && {
        lat: route.params.initialLat,
        lng: route.params.initialLng
    }

    const [selectedLocation, setSelectedLocation] = useState(initialLocation)

    const region = {
        latitude: initialLocation ? initialLocation.lat : -26.107567,
        longitude: initialLocation ? initialLocation.lng : 28.056702,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421
    }

    const selectLocationHandler = (event) => {
        if (initialLocation){
            return
        }
        const lat = event.nativeEvent.coordinate.latitude
        const lng = event.nativeEvent.coordinate.longitude

        setSelectedLocation({
            lat: lat,
            lng: lng
        })
    }

    // Since we are using this function in our save button, to stop infinite loops we need to use the callback function
    // By using the dependency array in the use callback, this function will only be recreated if those components state changes
    const savePickedLocationHandler = useCallback(() => {
        if (!selectedLocation){
            Alert.alert(
                'No Location Picked!',
                'Please select a location by tapping on the map before saving.'
            )
            return
        }
        navigation.navigate(
            'AddPlace',
            {
                pickedLat: selectedLocation.lat,
                pickedLng: selectedLocation.lng
            }
        )
    },[navigation, selectedLocation])

    useLayoutEffect(() => {
        // if we have an initial location, we are view a map and not adding one, so we don;t want the save icon
        if (initialLocation){
            return
        }
        navigation.setOptions({
            headerRight: ({tintColor}) => (
                <IconButton
                    icon={'save'}
                    size={24}
                    color={tintColor}
                    onPress={savePickedLocationHandler}
                />
            )
        })
    }, [navigation, savePickedLocationHandler, initialLocation])

    return (
        <MapView
            style={styles.map}
            initialRegion={region}
            onPress={selectLocationHandler}
        >
            {selectedLocation &&
                <Marker
                    title={'Picked Location'}
                    coordinate={{
                        latitude: selectedLocation.lat,
                        longitude: selectedLocation.lng
                    }}
                />
            }
        </MapView>
    )
}

const styles = StyleSheet.create({
    map: {
        flex: 1
    }
})

export default Map
