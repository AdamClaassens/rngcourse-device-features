import PlaceForm from "../components/places/PlaceForm";
import {InsertPlace} from "../util/database";


const AddPlace = ({navigation}) => {
    const createPlaceHandler = async (place) => {
        await InsertPlace(place)
        navigation.navigate(
            'AllPlaces'
        )
    }

    return (
        <PlaceForm onCreatePlace={createPlaceHandler}/>
    )
}

export default AddPlace
