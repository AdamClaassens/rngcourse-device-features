import {ScrollView, StyleSheet, View, Image, Text} from "react-native";
import OutlinedButton from "../components/ui/OutlinedButton";
import {Colors} from "../constants/colors";
import React, {useEffect, useState} from "react";
import {FetchPlaceDetails} from "../util/database";

const PlaceDetails = ({route, navigation}) => {
    const [fetchedPlace, setFetchedPlace] = useState()
    const showOnMapHandler = () => {
        navigation.navigate('Map', {
            initialLat: fetchedPlace.lat,
            initialLng: fetchedPlace.lng
        })
    }

    const selectedPlaceId = route.params.placeId

    useEffect(() => {
        // Use selectedPlaceId to fetch data for a single place
        const loadPlaceData = async () => {
            const place = await FetchPlaceDetails(selectedPlaceId)
            setFetchedPlace(place)
            navigation.setOptions({
                title: place.title
            })
        }

        loadPlaceData()
    }, [selectedPlaceId])

    if(!fetchedPlace){
        return (
            <View
                style={styles.fallback}
            >
                <Text>Loading Place Data...</Text>
            </View>
        )
    }

    return (
        <ScrollView>
            <Image
                style={styles.image}
                source={{uri: fetchedPlace.imageUri}}
            />
            <View
                style={styles.locationContainer}
            >
                <View
                    style={styles.addressContainer}
                >
                    <Text
                        style={styles.address}
                    >
                        {fetchedPlace.address}
                    </Text>
                </View>
                <OutlinedButton
                    icon={'map'}
                    onPress={showOnMapHandler}
                >
                    View on Map
                </OutlinedButton>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    fallback:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        height: '35%',
        minHeight: 300,
        width: '100%'
    },
    locationContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    addressContainer: {
        padding: 20
    },
    address: {
        color: Colors.primary500,
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 16
    }
})

export default PlaceDetails
