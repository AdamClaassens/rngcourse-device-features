import PlacesList from "../components/places/PlacesList";
import {useIsFocused} from "@react-navigation/native";
import {useEffect, useState} from "react";
import {FetchPlaces} from "../util/database";

const AllPlaces = ({route}) => {
    const [loadedPlaces, setLoadedPlaces] = useState([])

    const isFocussed = useIsFocused()

    useEffect(() => {
        const loadPlaces = async () => {
            const places = await FetchPlaces()
            setLoadedPlaces(places)
        }
        if(isFocussed){
            loadPlaces()
            // setLoadedPlaces(curPlaces => [...curPlaces, route.params.place])
        }
    },[isFocussed])

    return(
        <PlacesList places={loadedPlaces}/>
    )
}

export default AllPlaces
