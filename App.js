import {StatusBar} from 'expo-status-bar';
import {Text, View} from 'react-native';
import {NavigationContainer} from "@react-navigation/native";
import {createNativeStackNavigator} from "@react-navigation/native-stack";
import AllPlaces from "./screens/AllPlaces";
import AddPlace from "./screens/AddPlace";
import IconButton from "./components/ui/IconButton";
import {Colors} from "./constants/colors";
import Map from "./screens/Map";
import {useCallback, useEffect, useState} from "react";
import {Init} from "./util/database";
import * as SplashScreen from "expo-splash-screen";
import PlaceDetails from "./screens/PlaceDetails";

const Stack = createNativeStackNavigator()
export default function App() {
    const [dbinitialized, setDbinitialized] = useState(false)

    useEffect(() => {
        Init()
            .then(() => {
                setDbinitialized(true)
            })
            .catch((err) => {
                console.log(err)
            })
    }, [])

    const onLayoutRootView = useCallback(async () => {
        if (dbinitialized) {
            // This tells the splash screen to hide immediately! If we call this after
            // `setDbinitialized`, then we may see a blank screen while the app is
            // loading its initial state and rendering its first pixels. So instead,
            // we hide the splash screen once we know the root view has already
            // performed layout.
            await SplashScreen.hideAsync();
        }
    }, [dbinitialized]);

    if (!dbinitialized) {
        return null;
    }

    return (
        <>
            <StatusBar style={'dark'}/>
            <NavigationContainer>
                <Stack.Navigator
                    screenOptions={{
                        headerStyle: {
                            backgroundColor: Colors.primary500,
                        },
                        headerTintColor: Colors.gray700,
                        contentStyle: {
                            backgroundColor: Colors.gray700
                        }
                    }}
                >
                    <Stack.Screen
                        name={"AllPlaces"}
                        component={AllPlaces}
                        options={({navigation}) => ({
                            title: 'Your favorite places',
                            headerRight: ({tintColor}) => (
                                <IconButton
                                    icon={"add"}
                                    size={24}
                                    color={tintColor}
                                    onPress={() => navigation.navigate('AddPlace')}/>
                            )
                        })}
                    />
                    <Stack.Screen
                        name={"AddPlace"}
                        component={AddPlace}
                        options={{
                            title: 'Add a new place'
                        }}
                    />
                    <Stack.Screen
                        name={"Map"}
                        component={Map}
                    />
                    <Stack.Screen
                        name={"PlaceDetails"}
                        component={PlaceDetails}
                        options={{
                            title: 'Loading Place Details...'
                        }}
                    />
                </Stack.Navigator>
            </NavigationContainer>
        </>
    );
}

