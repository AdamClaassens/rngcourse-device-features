import * as SQLite from 'expo-sqlite'
import {Place} from "../models/place";

const database = SQLite.openDatabase('places.db')

const Init = () => {
    // Here we are building a promise function
    // We run the sql command in the promise function, this way we can catch the error as well as see the sesolved state.
    const promise = new Promise((resolve, reject) => {
        database.transaction((transactionObject) => {
            transactionObject.executeSql(
                `CREATE TABLE IF NOT EXISTS places (
                id INTEGER PRIMARY KEY NOT NULL,
                title TEXT NOT NULL,
                imageUri TEXT NOT NULL,
                address TEXT NOT NULL,
                lat REAL NOT NULL,
                lng REAL NOT NULL
            )`,
                [],
                () => {
                    resolve()
                },
                (_, error) => {
                    reject(error)
                }
            )
        })
    })
    return promise
}

const InsertPlace = (place) => {
    const promise = new Promise((resolve, reject) => {
        database.transaction((tx) => {
            tx.executeSql(
                `INSERT INTO places (title, imageUri, address, lat, lng) VALUES (?, ?, ?, ?, ?)`,
                [
                    place.title,
                    place.imageUri,
                    place.address,
                    place.location.lat,
                    place.location.lng
                ],
                // I am using _ to state that I am getting a vlue but I not not want to use it
                (_, result) => {
                    console.log(result)
                    resolve(result)
                },
                (_, error) => {
                    reject(error)
                }
            )
        })
    })
    return promise
}

const FetchPlaces = () => {
    const promise = new Promise((resolve, reject) => {
        database.transaction((tx) => {
            tx.executeSql(`SELECT * FROM places`,
                [],
                (_, result) => {
                    let places = []
                    for (const dp of result.rows._array) {
                        places.push(
                            new Place(
                                dp.title,
                                dp.imageUri,
                                {
                                    address: dp.address,
                                    lat: dp.lat,
                                    lng: dp.lng
                                },
                                dp.id
                            )
                        )
                    }
                    resolve(places)
                },
                (_, error) => {
                    reject(error)
                }
            )
        })
    })
    return promise
}

const FetchPlaceDetails = (id) => {
    const promise = new Promise((resolve, reject) => {
        database.transaction((tx) => {
            tx.executeSql(`SELECT * FROM places WHERE id = ?`,
                [id],
                (_, result) => {
                    resolve(result.rows._array[0])
                },
                (_, error) => {
                    reject(error)
                }
            )
        })
    })
    return promise
}

export {Init, InsertPlace, FetchPlaces, FetchPlaceDetails}
