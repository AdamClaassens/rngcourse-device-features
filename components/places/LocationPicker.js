import {View, StyleSheet, Alert, Image, Text} from "react-native";
import OutlinedButton from "../ui/OutlinedButton";
import {Colors} from "../../constants/colors";
import {getCurrentPositionAsync, useForegroundPermissions, PermissionStatus} from "expo-location";

import imagePreviewUrl, {GetAddress, GetMapPreview} from '../../util/location'
import {useEffect, useState} from "react";
import {useIsFocused, useNavigation, useRoute} from "@react-navigation/native";

const LocationPicker = ({onPickLocation}) => {
    const [pickedLocation, setPickedLocation] = useState()
    const [locationPermissionInformation, requestPermission] = useForegroundPermissions()

    // isFucussed will be false whenever we navigate away from this screen and truen when we come back to this screen
    // We can use this to force a re-render
    const isFocussed = useIsFocused()
    const navigation = useNavigation()
    const route = useRoute()

    useEffect(() => {
        // Check if we are focussing on theis screen (Navigating back to it from Map
        // Check if there are route parameters coming from our Map component.
        if (isFocussed && route.params) {
            // Set location params from Map component if location is picked manually.
            // We are sending these values from the map component and picking them up here.
            const mapPickedLocation = {
                lat: route.params.pickedLat,
                lng: route.params.pickedLng
            }
            setPickedLocation(mapPickedLocation)
        }
    }, [route, isFocussed])

    // Whenever the picked location changes we need pass that value to the PlaceForm component
    // We need to add the useCallback on the onPickLocation function to prevent infinite loop
    useEffect(() => {
        const handleLocation = async () => {
            if (pickedLocation) {
                const address = await GetAddress(
                    pickedLocation.lat,
                    pickedLocation.lng
                )
                // Here we merge our lat and lng object (pickedLocation) into a new object that also has the address key value pair
                onPickLocation({...pickedLocation, address: address})
            }
        }

        handleLocation()

    },[pickedLocation, onPickLocation])

    const verifyPermission = async () => {
        if (locationPermissionInformation.status === PermissionStatus.UNDETERMINED) {
            const permissionResponse = await requestPermission()

            return permissionResponse.granted
        }

        if (locationPermissionInformation.status === PermissionStatus.DENIED) {
            Alert.alert(
                'Insufficient Permissions!',
                'You need to grant location permissions to use this app'
            )

            return false
        }

        return true
    }
    const getLocationHandler = async () => {
        const hasPermission = await verifyPermission()
        if (!hasPermission) {
            return
        }

        const location = await getCurrentPositionAsync()
        setPickedLocation({
            lat: location.coords.latitude,
            lng: location.coords.longitude
        })
    }
    const pickOnMapHandler = () => {
        navigation.navigate('Map')
    }

    let locationPreview = <Text>No location picked yet.</Text>

    if (pickedLocation) {
        locationPreview = <Image
            style={styles.image}
            source={{
                uri: GetMapPreview(pickedLocation.lat,
                    pickedLocation.lng)
            }}
        />
    }

    return (
        <View>
            <View
                style={styles.mapPreview}
            >
                {locationPreview}
            </View>
            <View
                style={styles.actions}
            >
                <OutlinedButton
                    icon={'location'}
                    onPress={getLocationHandler}
                >
                    Locate User
                </OutlinedButton>
                <OutlinedButton
                    icon={'map'}
                    onPress={pickOnMapHandler}
                >
                    Pick on Map
                </OutlinedButton>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    mapPreview: {
        width: '100%',
        height: 200,
        marginVertical: 8,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary100,
        borderRadius: 4,
        // This allows the border radius to persist to the image in this container without adding the border radius to the image as well
        overflow: 'hidden'
    },
    actions: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    image: {
        width: '100%',
        height: '100%',
        // borderRadius: 4
    }
})

export default LocationPicker
