import {Alert, Image, Text, View, StyleSheet} from "react-native";
import {launchCameraAsync, useCameraPermissions, PermissionStatus} from "expo-image-picker";
import {useState} from "react";
import {Colors} from "../../constants/colors";
import OutlinedButton from "../ui/OutlinedButton";

const ImagePicker = ({onTakeImage}) => {
    const [pickedImage, setPickedImage] = useState()

    // This is part of the process to allow camera permission on IOS
    const [cameraPermissionInformation, requestPermission] = useCameraPermissions()

    // Here we check if the camera permissions have been granted previously
    const verifyPermission = async () => {
        if (cameraPermissionInformation.status === PermissionStatus.UNDETERMINED) {
            const permissionResponse = await requestPermission()

            return permissionResponse.granted
        }

        if (cameraPermissionInformation.status === PermissionStatus.DENIED) {
            Alert.alert(
                'Insufficient Permissions!',
                'You need to grant camera permissions to use this app'
            )

            return false
        }

        return true
    }
    const takeImageHandler = async () => {
        // Here we we run the function where we check if permission has been granted previously
        const hasPermission = await verifyPermission()
        // If permissions were not granted just return
        if (!hasPermission) {
            return
        }

        const image = await launchCameraAsync({
            allowsEditing: true,
            aspect: [16, 9],
            quality: 0.5
        })

        setPickedImage(image.assets[0].uri)
        onTakeImage(image.assets[0].uri)
    }

    let imagePreview = <Text>No image taken yet.</Text>

    if (pickedImage) {
        imagePreview = <Image style={styles.image} source={{uri: pickedImage}}/>
    }

    return (
        <View>
            <View
                style={styles.imagePreview}
            >
                {imagePreview}
            </View>
            {/* Here we use a custom built button */}
            <OutlinedButton
                icon={'camera'}
                onPress={takeImageHandler}
            >
                Take Image
            </OutlinedButton>
        </View>
    )
}

const styles = StyleSheet.create({
    imagePreview: {
        width: '100%',
        height: 200,
        marginVertical: 8,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary100,
        borderRadius: 4,
        overflow: 'hidden'
    },
    image: {
        width: '100%',
        height: '100%'
    }
})

export default ImagePicker
