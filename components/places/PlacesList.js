import {FlatList, Text, View, StyleSheet} from "react-native";
import PlaceItem from "./PlaceItem";
import {Colors} from "../../constants/colors";
import {useNavigation} from "@react-navigation/native";

const PlacesList = ({places}) => {
    const navigation = useNavigation()

    const selectPlacesHandler = (id) => {
        navigation.navigate('PlaceDetails', {
            placeId: id
        })
    }

    if (!places || places.length === 0) {
        return(
            <View style={styles.fallbackContainer}>
                <Text
                    style={styles.fallbackText}
                >
                    No places have been added yet.
                </Text>
            </View>
        )
    } else {
        return <FlatList
            style={styles.list}
            data={places}
            renderItem={({item}) =>
                // The place we are passing here will be our place Class/Object under models
                <PlaceItem place={item} onSelect={selectPlacesHandler}/>
            }
            keyExtractor={(item) => item.id}
        />
    }

}

const styles = StyleSheet.create({
    list: {
        margin: 24
    },
    fallbackContainer: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    fallbackText: {
        fontSize: 16,
        color: Colors.primary200
    }
})

export default PlacesList
